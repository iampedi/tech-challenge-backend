import { isNumber } from "../utils/isNumber.js";
import db from "./../lib/db.js";
import lendersRules from "./lender.rule.js";

export function validateInsert(data) {
  const {
    name,
    paymentFrequency,
    interestRate,
    monthlyFee,
    brokerFee,
    arrearsOnly,
  } = data;
  if (!name) {
    return "Field name cannot be empty";
  }
  if (!paymentFrequency) {
    return "Field paymentFrequency is not valid";
  }
  if (!interestRate || !isNumber(interestRate)) {
    return "Field interestRate is not valid";
  }
  if (!monthlyFee || !isNumber(monthlyFee)) {
    return "Field monthlyFee is not valid";
  }
  if (!brokerFee || !isNumber(monthlyFee)) {
    return "Field brokerFee is not valid";
  }
  if (typeof arrearsOnly !== "boolean") {
    return "Field arrearsOnly is not valid";
  }
}
export function insert(data) {
  const {
    name,
    paymentFrequency,
    interestRate,
    monthlyFee,
    brokerFee,
    arrearsOnly,
  } = data;
  db.data.lenders.push({
    name,
    paymentFrequency,
    interestRate,
    monthlyFee,
    brokerFee,
    arrearsOnly,
  });
  db.write();
  return db.data.lenders[db.data.lenders.length - 1];
}

export function calculateRepayments(amount, period) {
  const result = [];
  for (const lender of db.data.lenders) {
    const isValid = lendersRules.isValid({ ...lender, amount, period });
    if (!isValid) continue;
    const repayment = lendersRules.execute({ ...lender, amount, period });
    result.push({
      lender_name: lender.name,
      repayments: repayment,
    });
  }
  return result;
}
