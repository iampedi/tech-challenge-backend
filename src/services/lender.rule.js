import RuleEngine from "../lib/rule-engine.js";
import PMT from "../utils/pmt.js";

const monthsPerYear = 12;
const conditions = [
  (data) => data.paymentFrequency === "monthly",
  (data) => data.arrearsOnly === true,
];
const pmtRules = [
  {
    name: "InterestRate",
    conditions,
    outcome: (data) => {
      data.monthlyInterestRate = data.interestRate / monthsPerYear;
      return data.monthlyInterestRate;
    },
  },
  {
    name: "BrokerFee",
    conditions,
    outcome: (data) => {
      data.loadTermMonths = data.period * monthsPerYear;
      data.monthlyBrokerFee = 1 + data.brokerFee / data.loadTermMonths;
      return data.monthlyBrokerFee;
    },
  },
  {
    name: "PMT",
    conditions,
    outcome: (data) => {
      data.pmt = PMT(
        data.monthlyInterestRate,
        data.loadTermMonths,
        data.amount
      );
      return data.pmt;
    },
  },
  {
    name: "Repayment",
    conditions,
    outcome: (data) => {
      data.repayment = (data.pmt - data.monthlyFee) * data.monthlyBrokerFee;
      return data.repayment;
    },
  },
];

export default new RuleEngine(pmtRules);
