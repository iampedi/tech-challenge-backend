import express from "express";
import "./lib/db.js";
import {
  calculateRepayments,
  insert as insertLender,
  validateInsert as validateInsertLender,
} from "./services/lender.service.js";

const app = express();
const port = process.env.PORT || 3000;
app.use(express.json());

app.get("/", (req, res) => {
  res.send("Hello world");
});

app.get("/lenders/repayment", (req, res) => {
  const { amount, repaymentPeriod } = req.query;
  return res.json({ ...calculateRepayments(amount, repaymentPeriod) });
});

app.post("/lenders", (req, res) => {
  const validation = validateInsertLender(req.body);
  if (validation) {
    res.status(400).json({ error: true, message: validation });
  }
  return res.status(201).json(insertLender(req.body));
});

app.listen(port, () => {
  console.info(`Server is running on Port ${port}`);
});
