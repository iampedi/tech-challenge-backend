import { join, dirname } from "path";
import { Low, JSONFile } from "lowdb";
import { fileURLToPath } from "url";

const __dirname = dirname(fileURLToPath(import.meta.url));

// Use JSON file for storage
const file = join(__dirname, "./../db.json");
const adapter = new JSONFile(file);
const db = new Low(adapter);

// Read data from JSON file, this will set db.data content
await db.read();

// If file.json doesn't exist, db.data will be null
// Set default data
// db.data = db.data || { posts: [] } // Node < v15.x
db.data ||= {
  lenders: [
    {
      id: "lenderA",
      name: "Lender A",
      paymentFrequency: "monthly",
      interestRate: 0.055,
      monthlyFee: 40,
      brokerFee: 0.02,
      arrearsOnly: true,
    },
    {
      id: "lenderB",
      name: "Lender B",
      paymentFrequency: "monthly",
      interestRate: 0.06,
      monthlyFee: 50,
      brokerFee: 0.017,
      arrearsOnly: true,
    },
  ],
};

// Finally write db.data content to file
await db.write();

export default db;
