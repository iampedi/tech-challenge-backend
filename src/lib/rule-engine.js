export default class RuleEngine {
  constructor(rules) {
    this._rules = rules;
  }

  _validateConditions(rule, target) {
    const conditionChecks = [];
    for (const condtion of rule.conditions) {
      conditionChecks.push(condtion(target));
    }

    return conditionChecks.every((result) => result === true);
  }

  _applyRules(rule, target) {
    if (this._validateConditions(rule, target)) {
      return rule.outcome(target);
    }
  }

  isValid(target) {
    if (Array.isArray(this._rules)) {
      const result = [];
      for (const rule of this._rules) {
        return this._validateConditions(rule, target);
      }
      return result;
    } else {
      if (typeof this._rules === "object") {
        return this._validateConditions(this._rules, target);
      }
    }
  }

  execute(target) {
    if (Array.isArray(this._rules)) {
      const result = [];
      for (const rule of this._rules) {
        result.push({
          key: rule.name,
          result: this._applyRules(rule, target),
        });
      }
      return result;
    } else {
      if (typeof this._rules === "object") {
        return {
          key: this._rules.name,
          result: this._applyRules(this._rules, target),
        };
      }
    }
  }
}
