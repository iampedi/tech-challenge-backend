import { isNumber } from "./isNumber.js";
// Source https://github.com/FormulaPages/pmt/blob/120ab76744bdf69dfd517e9c59f1c66498959360/PMT.es6
export default function PMT(rate, periods, present, future = 0, type = 0) {
  if (rate === 0) {
    return -((present + future) / periods);
  } else {
    var term = Math.pow(1 + rate, periods);
    if (type === 1) {
      return -(
        ((future * rate) / (term - 1) + (present * rate) / (1 - 1 / term)) /
        (1 + rate)
      );
    } else {
      return -(
        (future * rate) / (term - 1) +
        (present * rate) / (1 - 1 / term)
      );
    }
  }
}
