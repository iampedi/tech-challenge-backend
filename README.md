# Repayment Calculator

**Loom Video**
https://www.loom.com/share/d91cdd90b2bc41cab191a0a93cbbbb93

## How To Run

You can run the Repayment Calculator using the below command.

Start with

```
npm install
```

And then

```
npm run start
```

`PORT` environment variable is available.

## How to send requests

#### Get Repayments

`GET /lenders/repayment/?amount=20000&repaymentPeriod=5`

#### Insert Lender

`POST /lenders`

```json
{
  "name": "New Lender",
  "paymentFrequency": "monthly",
  "interestRate": 0.065,
  "monthlyFee": 70,
  "brokerFee": 0.026,
  "arrearsOnly": true
}
```

## documentation on the solution

As soon as the app starts, `db.js` will seed our database with our default lenders.

I have created `RuleEngine.js` class which accepts an array or object of rules. In this class, I have a simple code that execute rules based on the given `outcome` function.

For instance, below is how I create a simple rule to calculate `PMT`

```javascript
const pmtRules = [
  {
    name: "PMT",
    conditions: [
      (data) => data.paymentFrequency === "monthly",
      (data) => data.arrearsOnly === true,
    ],
    outcome: (lender) => {
      return (
        (PMT(getInterestRate(lender.interestRate), loadTermMonths, amount) -
          lender.monthlyFee) *
        getBrokerFee(lender.brokerFee)
      );
    },
  },
];
const ruleEngine = new RuleEngine(pmtRules);
```

Once the rules are registered in our Rule Engine, we can use the `execute` method to calculate the repayments.
`const repayment = ruleEngine.execute(lender);`

Each lender will be checked against the given criteria to check if they can provide service. The `validate` is accessible and also lenders will be validated agains the given conditions in excution phase as well.

## how long did you spend creating the application

I started coding it at 8:40 PM and it finished at 10:15 PM.

## Assumptions

- All requests are made for monthly payments.
- Fee calculation for all lenders is monthly.
- Brokers' fee is yearly.
- Repayment is monthly for all lenders.

## Any improvements you would make

- I would implement the code using Typescript.
- I would make paymentType a queryString as well as amount and paymentPeriod.

## Any queries that need resolving

I had to make a slight change in the given rules' order. I calculate the broker fee before the monthly fee. In my implementation monthly fee is a part of the final repayment calculation and does not have its own rule. It makes me think whether I have implemented rules right or I have misunderstood the test description.
